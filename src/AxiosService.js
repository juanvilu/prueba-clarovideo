import axios from 'axios';

const ApiLista = axios.create ({
    baseURL: 'https://mfwkweb-api.clarovideo.net/services/content/list?api_version=v5.86&authpn=webclient&authpt=tfg1h3j4k6fd7&format=json&region=mexico&device_id=web&device_category=web&device_model=web&device_type=web&device_so=chrome&device_manufacturer=generic&HKS=aqi3uhrmvuecuh30l00i69sll1&quantity=40&order_way=DESC&order_id=200&level_id=GPS&from=0&node_id=43864&user_id=6512966'
});

const ApiDetalle = axios.create({    
    baseURL: 'https://mfwkweb-api.clarovideo.net/services/content/data?device_id=web&device_category=web&device_model=web&device_type=web&format=json&device_manufacturer=generic&authpn=webclient&authpt=tfg1h3j4k6fd7&api_version=v5.86&region=mexico&HKS=aqi3uhrmvuecuh30l00i69sll1&user_id=6512966'
});

export default {
    lista_scifi: ApiLista,
    detalle_scifi: ApiDetalle
};