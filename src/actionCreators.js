const initPeliculas = (peliculas) => {
    return {
        type: 'INIT_PELICULAS',
        peliculas
    }
};

const buscarPelicula = titulo => {
    return {
        type: 'NUEVA_BUSQUEDA',
        titulo
    };
};

const filtrarPeliculas = () => {
    return {
        type: 'FILTRAR_PELICULAS'        
    };
};

const resetBusquedaPeliculas = () => {
    return {
        type: 'RESET_BUSQUEDA_PELICULAS'
    }
};

const mostrarCardDetalle = pelicula => {
    return {
        type: 'MOSTRAR_CARD_DETALLE',
        pelicula
    }
};

const mostrarListaPeliculas = () => {
    return {
        type: 'MOSTRAR_LISTA_PELICULAS'
    }
};

const peticionDetallePelicula = id => {
    return {
        type: "PETICION_DETALLE_PELICULA",
        id
    }
};

export {
    initPeliculas,
    buscarPelicula,
    filtrarPeliculas,
    resetBusquedaPeliculas,
    mostrarCardDetalle,
    mostrarListaPeliculas,
    peticionDetallePelicula
};