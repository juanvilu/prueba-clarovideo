import Servicio from './AxiosService';

export default {
    gen_scifi: () => {
        Servicio.lista_scifi.get('/')
                .then(peliculas => {
                    // console.log(peliculas.data.response.groups);
                    return peliculas.data.response.groups;
                })
                .catch(error => {
                    console.error('ERROR SCIFI');
                });
    }
}