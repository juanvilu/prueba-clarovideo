import React from 'react';
import './App.css';
import logo from './clarovideo-logo-sitio.svg';
import Contenedor from './Componentes/Contenedor';

function App() {
  return (
    <div className="App">      
      <header className="App-header">
        <div className="navbar navbar-dark">
          <div className="container">
            <a href="" className="navbar-brand">
              <img src={logo} className="App-logo" alt="logo" />
            </a>                    
          </div>
        </div>        
      </header>
      
      <main>
        <Contenedor></Contenedor>
      </main>
    </div>
  );
}

export default App;
