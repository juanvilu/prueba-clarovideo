// group_id=780840

import Servicio from './AxiosService';

export default {
    detalle_pelicula: (id) => {
        Servicio.detalle_scifi.get('/', {params: {group_id: id}})
                .then(pelicula => {
                    return pelicula.data.response.group.common;
                })
                .catch(error => {
                    console.error('ERROR SCIFI');
                });
    }
}