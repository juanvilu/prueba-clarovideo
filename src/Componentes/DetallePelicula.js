// https://www.clarovideo.com/mexico/gen_scifi
// https://mfwkweb-api.clarovideo.net//services/content/list?api_version=v5.86&authpn=webclient&authpt=tfg1h3j4k6fd7&format=json&region=mexico&device_id=web&device_category=web&device_model=web&device_type=web&device_so=chrome&device_manufacturer=generic&HKS=aqi3uhrmvuecuh30l00i69sll1&quantity=40&order_way=DESC&order_id=200&level_id=GPS&from=0&node_id=43864&user_id=6512966
// https://www.clarovideo.com/mexico/vcard/gen_scifi/Exterminio/776749
// https://fontawesome.com/icons?d=gallery&q=back

// Eva
// https://mfwkweb-api.clarovideo.net/services/content/data?device_id=web&device_category=web&device_model=web&device_type=web&format=json&device_manufacturer=generic&authpn=webclient&authpt=tfg1h3j4k6fd7&api_version=v5.86&region=mexico&HKS=aqi3uhrmvuecuh30l00i69sll1&user_id=6512966&group_id=780840

import React from 'react';
import { connect } from 'react-redux';
import { faArrowLeft } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { mostrarListaPeliculas } from '../actionCreators';

const DetallePelicula= ({pelicula, mostrarCardDetalle, mostrarListaPeliculas}) => {
    return (
        <div style={
            {
                backgroundImage: `url('${pelicula.image_background}')`, 
                imageSize: 'cover'
            }} className={mostrarCardDetalle ? 'container mt-4 p-4' : 'container mt-4 toggle_hide p-4'}>
            <div className="row">
                <div className="col text-left">
                    <button className="btn btn-outline-dark text-white" onClick={() => mostrarListaPeliculas()}>
                        <FontAwesomeIcon icon={faArrowLeft}/> Regresar
                    </button>
                </div>
            </div>
            <div className="row mt-4">
                <div className="col text-left">
                    <h3 className="text-white">{pelicula.title} ({pelicula.year})</h3>
                </div>
            </div>
            <div className="row mt-4">
                <div className="col-md-4">
                <img src={ pelicula.image_small } className="card-img" alt=""></img>
                </div>
                <div className="col-md-8">                       
                    <p className="text-white text-left">{pelicula.description_large}</p>
                </div>
            </div>                               
        </div>
    )
}

const mapStateToProps = state => {    
    return {
        mostrarCardDetalle: state.mostrarCardDetalle, 
        pelicula: state.pelicula
    };
};

const mapDispatchToProps = dispatch => {
    return {
        mostrarListaPeliculas() {
            dispatch(mostrarListaPeliculas());
        }
    }
}

export default connect (mapStateToProps, mapDispatchToProps)(DetallePelicula);