// https://www.youtube.com/watch?v=RZNNu2pO49g&list=PLxyfMWnjW2kuyePV1Gzn5W_gr3BGIZq8G
// https://www.youtube.com/watch?v=OXWn4XiDUmw

// https://github.com/LINKIWI/react-elemental
// https://es.redux.js.org/docs/introduccion/ecosistema.html

// https://www.youtube.com/watch?v=67ASQxnnyFE

import React, { Component } from 'react';
import BusquedaPeliculas from '../Componentes/BusquedaPeliculas';
import CardPelicula from './CardPelicula';
import store from '../store';

class ListaGeneros extends Component {
    constructor () {
        super();
        this.state = {            
            peliculasFiltradas: [],
            busqueda: '',
            mostrarListaPeliculas: null           
        };                

        store.subscribe(() => {
            this.setState({                
                peliculasFiltradas: store.getState().peliculasFiltradas,
                mostrarListaPeliculas: store.getState().mostrarListaPeliculas
            });
        });
        
        this.mostrarPeliculas = this.mostrarPeliculas.bind(this);
    }

    mostrarPeliculas () {
        return this.state.peliculasFiltradas.map((pelicula,i) => {
            return <CardPelicula pelicula={ pelicula } key={i}></CardPelicula>
        });
    }   

    render () {                      
        return (
            <div className={this.state.mostrarListaPeliculas ? 'container mt-4' : 'container mt-4 toggle_hide'}>
                <BusquedaPeliculas></BusquedaPeliculas>
                
                <div id="lista" className="row mt-3 main">
                   { this.mostrarPeliculas() } 
                </div>                
            </div>
        )
    }
}

export default ListaGeneros;