import React, { Component } from 'react';
import store from '../store';
import { mostrarCardDetalle, peticionDetallePelicula } from '../actionCreators';

class CardPelicula extends Component {
    render () {        
        return (
            <div className="col-md-4 hover col-sm-6 col-xs-1 my-2">
                <div className="card border-dark text-white" onClick={() => this.mostrarCardDetalle(this.props.pelicula)}>
                    <img src={ this.props.pelicula.image_small } className="card-img" alt=""></img>
                    <div className="card-image">
                        <div className="card-img-overlay">

                        </div>
                    </div>
                </div>
            </div>
        )
    }

    mostrarCardDetalle (pelicula) {         
        store.dispatch(peticionDetallePelicula(pelicula.id));
        store.dispatch(mostrarCardDetalle(pelicula));
    }
}

export default CardPelicula;