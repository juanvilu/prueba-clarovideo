import React, { Component } from 'react';
import store from '../store';
import { buscarPelicula, filtrarPeliculas, resetBusquedaPeliculas } from '../actionCreators';

class BusquedaPeliculas extends Component {        
    constructor () {
        super();

        store.subscribe(() => {
            this.setState({
                peliculas: store.getState().peliculas
            });
        });

        this.buscarPelicula = this.buscarPelicula.bind(this);
        this.filtrarPeliculas = this.filtrarPeliculas.bind(this);
    }

    render () {
        return (
            <div className="row">
                <form className="form-inline mx-auto my-lg-0" onSubmit={this.filtrarPeliculas} onReset={this.resetBusquedaPeliculas}>
                    <input 
                        name="busqueda"
                        className="form-control mr-sm-2"
                        type="search"
                        placeholder="¿Qué película buscas...?"
                        aria-label="Buscar"
                        onChange={this.buscarPelicula}                             
                    />
                    <button
                        className="btn btn-outline-info my-2 my-sm-0"
                        type="submit"                            
                    >
                        Buscar
                    </button>
                </form>
            </div>
        );
    }

    buscarPelicula (e) {        
        if (!e.target.value || e.target.value.length < 3) {
            this.resetBusquedaPeliculas();
            return;
        }

        store.dispatch(buscarPelicula(e.target.value));
    }

    filtrarPeliculas (e) {
        e.preventDefault();
        e.stopPropagation();

        store.dispatch(filtrarPeliculas());
    }

    resetBusquedaPeliculas () {
        store.dispatch(resetBusquedaPeliculas());
    }
}

export default BusquedaPeliculas;