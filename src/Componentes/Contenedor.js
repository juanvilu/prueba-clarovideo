import React, { Component } from 'react';
import Servicio from '../AxiosService';
import store from '../store';
import ListaGeneros from '../Componentes/ListaGeneros';
import DetallePelicula from '../Componentes/DetallePelicula';
import { initPeliculas } from '../actionCreators';

class Contenedor extends Component {
    constructor ()        {
        super();        
        
        Servicio.lista_scifi.get('/')
        .then(peliculas => {
            this.initPeliculas(peliculas.data.response.groups);
        });
    }
    render () {
        return (
            <div>
                <ListaGeneros></ListaGeneros>
                <DetallePelicula></DetallePelicula>
            </div>
        );    
    }
    initPeliculas(peliculas) {
        store.dispatch(initPeliculas(peliculas));
    }
}

export default Contenedor;