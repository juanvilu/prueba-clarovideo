import React from 'react';
import { connect } from 'react-redux';
import { buscarPelicula, filtrarPeliculas, resetBusquedaPeliculas } from '../actionCreators';

const BusquedaPeliculas = ({filtrarPeliculas, resetBusquedaPeliculas, buscarPelicula}) => {
    return (
        <div className="row">
            <form className="form-inline mx-auto my-lg-0" onSubmit={() => filtrarPeliculas} onReset={() => resetBusquedaPeliculas}>
                <input 
                    name="busqueda"
                    className="form-control mr-sm-2"
                    type="search"
                    placeholder="¿Qué película buscas...?"
                    aria-label="Buscar"
                    onChange={() => buscarPelicula}
                />
                <button
                    className="btn btn-outline-info my-2 my-sm-0"
                    type="submit"                            
                >
                    Buscar
                </button>
            </form>
        </div>
    );
}

const mapStateToProps = state => {
    return {
        busqueda: state.busqueda,
        peliculas: state.peliculas,
        peliculasFiltradas: state.peliculasFiltradas
    }
};

const mapDispatchToProps = dispatch => {
    return {
        buscarPelicula (e) {            
            e.preventDefault();
            e.stopPropagation();

            if (!e.target.value || e.target.value.length < 3) {
                dispatch(resetBusquedaPeliculas());
                return false;
            }
            dispatch(buscarPelicula(e.target.value));
            return false;
        },
        filtrarPeliculas (e) {            
            e.preventDefault();
            e.stopPropagation();

            dispatch(filtrarPeliculas());
            return false;
        },
        resetBusquedaPeliculas (e) {
            e.preventDefault();
            e.stopPropagation();
            dispatch(resetBusquedaPeliculas());
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(BusquedaPeliculas);