import { createStore } from 'redux';
import ApiDetallePelicula from './ApiDetallePelicula';

const reducer = (state, action) => {
    if (action.type === 'INIT_PELICULAS') {        
        return {
            ...state,
            peliculas: action.peliculas,
            peliculasFiltradas: action.peliculas,
            mostrarCardDetalle: false,
            mostrarListaPeliculas: true,
            pelicula: {image_small: '0603-Mirus-Gallery-290x163.jpg'}
        }
    } else if (action.type === 'NUEVA_BUSQUEDA') {
        return {
            ...state,
            titulo: action.titulo
        }
    } else if (action.type === 'FILTRAR_PELICULAS') {        
        return {
            ...state,
            peliculasFiltradas: state.peliculas.filter((pelicula, i) => {                        
                return pelicula.title.includes(state.titulo);
            })
        };
    } else if (action.type === 'RESET_BUSQUEDA_PELICULAS') {
        return {
            ...state,
            peliculasFiltradas: state.peliculas
        };
    } else if (action.type === 'MOSTRAR_CARD_DETALLE') {
        return {
            ...state,
            mostrarCardDetalle: true,
            mostrarListaPeliculas: false,
            pelicula: action.pelicula
        }
    } else if (action.type === 'MOSTRAR_LISTA_PELICULAS') {
        return {
            ...state,
            mostrarCardDetalle: false,
            mostrarListaPeliculas: true
        }
    } else if (action.type === 'PETICION_DETALLE_PELICULA') {
        return {
            ...state,
            pelicula: ApiDetallePelicula.detalle_pelicula(action.id)
        };
    }

    return state;
};

export default createStore(reducer, {titulo: '', peliculas: [], peliculasFiltradas: [], listaVisible: true, pelicula: {image_small: null}});